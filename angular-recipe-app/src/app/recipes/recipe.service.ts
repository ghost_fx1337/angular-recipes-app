import { ShoppingListService } from './../shopping-list/shopping-list.service';
import { Recipe } from './recipe.model';
import { EventEmitter, Injectable } from '@angular/core';
import { Ingredient } from '../shared/ingredient.model';

@Injectable()
export class RecipeService {
  recipeSelected = new EventEmitter<Recipe>();

  constructor(private readonly slService: ShoppingListService) {}

  private recipes: Recipe[] = [
    new Recipe(
      'Kebab with fries',
      'this is a test desc for kebab',
      'https://thumbs.dreamstime.com/z/kebab-fries-plate-french-lettuce-tomatoes-red-cabbage-81051839.jpg',
      [new Ingredient('Meat', 1), new Ingredient('French Fries', 20)]
    ),
    new Recipe(
      'Pasta',
      'this is a test desc for pasta',
      'https://lilluna.com/wp-content/uploads/2017/10/penne-pasta-resize-3-500x500.jpg',
      [new Ingredient('Pasta', 1), new Ingredient('Cheese', 1)]
    ),
  ];

  getRecipes() {
    return this.recipes.slice();
  }

  getRecipe(id: number) {
      return this.recipes[id];
  }

  addIngredientsToShoppingList(ingredients: Ingredient[]) {
    this.slService.addIngredients(ingredients);
  }
}
